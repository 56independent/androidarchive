package com.example.tictac;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void playButtonClick(View view) {
        Intent intent = new Intent( this, player_setup.class);
        startActivity(intent);
    }
    public void skipAll(View view){
        String player1 = "player1";
        String player2 = "player2";

        Intent intent = new Intent(this, gamedisplay.class);
        Intent player_names = intent.putExtra("PLAYER_NAMES",
                new String[] {player1, player2});
        startActivity(intent);
    }
}