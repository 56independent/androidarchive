package com.example.tictac;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class player_setup extends AppCompatActivity {
    private EditText player1;
    private EditText player2;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.player_setup);

        player1 = findViewById(R.id.editTextTextPersonName);
        player2 = findViewById(R.id.editTextTextPersonName2);
//        player1 = "sam";
//        player2 = "mary";
    }

    public void submitButtonClick(View view) {
        String player1Name = player1.toString();
        String player2Name = player2.toString();

        Intent intent = new Intent(this, gamedisplay.class);
        Intent player_names = intent.putExtra("PLAYER_NAMES",
                new String[] {player1Name, player2Name});
//        startActivity(intent);
        startActivity(player_names);
    }
}
